# O curso de Engenharia de Computação

A graduação em Engenharia de Computação tem duração média de 5 anos (10 semestres), geralmente na modalidade bacharelado, podendo ser oferecida em período integral ou turnos, presencial ou a distância. Boa infraestrutura de acesso à tecnologia e informática são essenciais para a faculdade que oferece este curso.

Nas melhores faculdades de Engenharia de Computação, a matriz curricular inicia com conhecimentos gerais das Engenharias, como Matemática, Física, Cálculos, Computação, Mecânica, Estatística e Geometria Analítica. Assim, o aluno começa a ter uma base da área, para posteriormente ser apresentado à disciplinas mais especializadas.

Desta forma, posteriormente são introduzidas matérias específicas da Engenharia de Computação, como Circuitos Elétricos, Programação, Sinais e Sistemas, Eletrônica Digital, Engenharia de Software e Sistemas Embarcados. Para atuar como engenheiro da computação o estudante deve gostar de matérias na área de exatas e tecnologias. É um curso que exige muita dedicação e estudo. Oportunidades de estágio supervisionado garantem a aplicação na prática dos conhecimentos adquiridos em sala de aula

O curso de Engenharia de Computação habilita a pessoa para trabalhar com o desenvolvimento e planejamento de softwares e hardwares. Com essa formação, ela poderá projetar, programar e gerenciar sistemas computacionais, além de criar e projetar computadores, periféricos e circuitos.


De acordo com o Ministério da Educação (MEC), a obtenção de qualquer diploma em Engenharia está condicionada à produção de um trabalho de conclusão de curso (TCC) e estágio obrigatório supervisionado de no mínimo 160 horas.

![](https://www.tuiuti.edu.br/hubfs/ci%C3%AAncia%20da%20computa%C3%A7%C3%A3o%202.jpg) 

[Inicio](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/tree/master)

[<--Pag. Anterior](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Futuro_da_Engenharia_da_Computação.md) - [Proxima Pag.-->](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/Referências.md)
