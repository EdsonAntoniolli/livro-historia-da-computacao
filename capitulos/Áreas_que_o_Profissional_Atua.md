# Áreas de Atuação de um Engenheiro da Computação

A Engenharia da Computação é o campo da engenharia responsável por projetar e construir novos computadores, desenvolver, implementar e testar sistemas de computação, de automação e comunicação. Nessa área o profissional conta com um mercado de trabalho com boas oportunidades devido à expansão e avanço das novas tecnologias. Quem deseja se destacar deve estar em constante atualização profissional para acompanhar as transformações científicas relacionadas a área tecnológica.

Ao longo do curso de Engenharia da Computação o estudante tem acesso a qualificação necessária para atuar em diversas áreas. Há diferentes oportunidades para desenvolver uma carreira sólida. Confira algumas delas:

## Projeção de equipamentos de informática:

Nessa área, o engenheiro da computação será responsável por projetar e construir peças de informática que compõem novos computadores, como processadores, periféricos, etc. Com as rápidas transformações tecnológicas, esse é um campo importante para o profissional, já que as peças, ferramentas e equipamentos são constantemente atualizadas. 

## Desenvolvimento de sistemas:

Outra área de atuação que o engenheiro de computação pode optar por seguir é o desenvolvimento de programas. O profissional terá habilidades para manter sistemas de computação, implementar software para sistemas de comunicação e aplicações de softwares e serviços. Dessa forma, poderá contribuir para o monitoramento e controle de diferentes atividades através de dispositivos e equipamentos apropriados.

## Redes de Computadores:

Projeção, implantação, gerenciamento e manutenção de redes de computadores também são atividades que podem ser desenvolvidas pelo engenheiro de computação. As redes de computadores são importantes para a troca de informações e recursos em diferentes máquinas, interligadas entre si por sistema de comunicação. 

## Controle e automação:

Área importante para o setor industrial, controle e automação lida com a automatização de tarefas e processos. O engenheiro da computação pode atuar nesse segmento com o desenvolvimento de robôs, aparelhos e sistemas.


# Média salarial na Engenharia da Computação:

A remuneração para quem atua com a Engenharia da Computação varia de acordo com a região do país e a qualificação profissional do engenheiro. De modo geral, os ganhos salariais estão em torno de R$ 5 mil, segundo o Caged (Cadastro Geral de Empregados e Desempregados). Os valores podem aumentar com serviços freelancer, consultoria, entre outras. A depender do porte da organização e do cargo ocupado pelo profissional, o salário pode ultrapassar R$ 9 mil, de acordo com o site Trabalha Brasil (Sine). 

[Inicio](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao)

[<-- Pag. Anterior](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_que_é_Engenharia_da_Computação.md) - [Proxima Pag. -->](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Futuro_da_Engenharia_da_Computação.md)
