1. WIKIPÉDIA. Engenharia de computação. 2021. Disponível em: https://pt.wikipedia.org/wiki/Engenharia_de_computação. Acesso em: 17 ago. 2021.

1. UNIJORGE. Engenharia da Computação é tendência entre profissões. 2020. Disponível em: https://blog.unijorge.edu.br/engenharia-da-computacao-e-tendencia-entre-profissoes-2/. Acesso em: 17 ago. 2021.

1. EDUCA+BRASIL. Engenharia da Computação: áreas de atuação do profissional: saiba como é o mercado de trabalho nessa área da engenharia. Saiba como é o mercado de trabalho nessa área da engenharia. 2019. Disponível em: https://www.educamaisbrasil.com.br/educacao/carreira/engenharia-da-computacao-areas-de-atuacao-do-profissional. Acesso em: 17 ago. 2021.

1. BRASILEIRO, Exercito. Engenharia de Computação - Graduação. Disponível em: http://www.comp.ime.eb.br/graduacao/historico/. Acesso em: 17 ago. 2021.

1. MULTIVIX. Engenharia de Computação: formação e mercado de trabalho rumo ao futuro. 2019. Disponível em: https://multivix.edu.br/blog/engenharia-de-computacao-formacao-e-mercado-de-trabalho-rumo-ao-futuro/. Acesso em: 17 ago. 2021.

1. ENGENHEIRO. Engenharia da Computação. 2013. TravelBR Turismo. Disponível em: https://www.engenheiro.com.br/engenharias-tecnologicas/engenharia-da-computacao.html. Acesso em: 17 ago. 2021.

[Inicio](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/tree/master)

[<--Pag. Anterior](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Curso_de_Engenharia_da_Computação.md)
