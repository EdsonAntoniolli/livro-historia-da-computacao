# O Futuro da profissão

Cozinhar, malhar, escrever, comprar e se comunicar. Atualmente, não dá mais para pensar em qualquer tarefa rotineira sem contar com a presença da tecnologia. Para executar qualquer função, seja ela simples ou complexa, podemos recorrer às facilidades dos aplicativos, sites, redes sociais ou similares, o que nos leva a perceber que o futuro tende a valorizar, precisar e trazer ainda mais oportunidades para aqueles que ajudam na criação de grande parte desses adventos tecnológicos: o profissional de Engenharia da Computação.  

Responsáveis por projetar e construir computadores, atuar tanto no hardware quanto no software de novas máquinas, desenvolver, implementar e testar sistemas de computação e automação industrial e robótica, criar programas de computadores e aplicativos de celular ou ainda atuar em projetos voltados para a otimização de sistemas de comunicação, o engenheiro da computação tem um vasto e muito promissor mercado de trabalho, que oferece oportunidades para aqueles que desejam atuar na indústria, em empresas de tecnologia, no ensino, na pesquisa, em consultoria e assessoria, ou ainda de forma independente.  

# Oportunidades e crescimento na Engenharia da Computação

Considerada como uma das áreas mais promissoras no cenário atual, os profissionais de Engenharia da Computação aparecem como grandes apostas para o futuro do mercado de trabalho, situação que tende a crescer graças à ascensão de grandes empresas do ramo tecnológico e a necessidade de transformação digital de empresas tradicionais.  

De acordo com uma pesquisa do LinkedIn, divulgada em janeiro de 2020, a área de tecnologia da informação já aparece como uma das que mais contrata atualmente. Dentre os profissionais com maiores chances, vemos aqueles com carreiras relacionadas à graduação, como Engenheiro(a) de Cibersegurança, Engenheiro(a) de Dados e Especialista em Inteligência Artificial.  

# Grande demanda, grandes salários e grandes exigências

Com a crescente necessidade de novas tecnologias e aplicabilidades, as oportunidades para os profissionais também aumentam, o que abre ainda mais portas para aqueles que desejam atuar em diversos segmentos dentro da área de tecnologia. Prova disso é que o setor de Engenharia da Computação continua em expansão mesmo em tempos de crise, como a situação da pandemia do Covid-19. A necessidade de manter o isolamento social para combater a disseminação do vírus alterou as rotinas de trabalho, estudo e lazer de todos, no mundo inteiro, fazendo com que nós dependêssemos, ainda mais, da internet, dos recursos tecnológicos e, é claro, dos profissionais da área.  

Figurando entre as profissões que já são bastante demandadas e que deverão aumentar ainda mais no futuro, a Engenharia da Computação domina inclusive as carreiras com maiores salários da área de tecnologia e já se mostra como a profissão com maior tendência de crescimento.  

Com tudo isso, o mercado de trabalho para os engenheiros da computação se tornou um terreno fértil de oportunidades, mas também se vê mais exigente. Além do talento e da criatividade, os profissionais precisam apresentar boa formação, boas competências e habilidades para a atuação diferenciada no mercado de trabalho. Aqueles que buscam cursos de especialização e MBA são ainda mais valorizados, estimulando a criação de novas frentes de trabalho com grande potencial para absorção dos profissionais da área. 

[Inicio](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/tree/master)

[<--Pag. Anterior](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/Áreas_que_o_Profissional_Atua.md) - [Proxima Pag.-->](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Curso_de_Engenharia_da_Computação.md)
