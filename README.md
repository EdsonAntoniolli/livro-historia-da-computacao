# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. 
O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão listados abaixo.

## Índice

1. [O que é a Engenharia da Computação](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_que_é_Engenharia_da_Computação.md)
1. [Áreas de Atuação do Engenheiro da Computação](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/Áreas_que_o_Profissional_Atua.md)
1. [O Futuro da Engenharia da Computação](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Futuro_da_Engenharia_da_Computação.md)
1. [O curso de Engenharia da Computação](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/O_Curso_de_Engenharia_da_Computação.md)
1. [Referências](https://gitlab.com/EdsonAntoniolli/livro-historia-da-computacao/-/blob/master/capitulos/Referências.md)

# Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9295854/avatar.png?width=400)  | Edson Antoniolli Junior | EdsonAntoniolli | [Edson.an.jr@gmail.com](mailto:Edson.an.jr@gmail.com)

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9328784/avatar.png?width=400) | Ketlin Melissa Wierzynski | Ketlinwierzynski | [Ketlinwierzynski@alunos.utfpr.edu.br](mailto:Ketlinwierzynski@alunos.utfpr.edu.br)
